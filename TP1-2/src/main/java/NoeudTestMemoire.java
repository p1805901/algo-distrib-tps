import io.jbotsim.core.Node;

public class NoeudTestMemoire extends Node {
  private int compteur;

  public void onStart() {
    compteur = 0;
  }

  public void onSelection() {
    compteur++;
    System.out.println("Le noeud numéro " + this.getID() + " a été sélectionné " + compteur + " fois !");
  }
}
