import io.jbotsim.core.Color;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

// Algo, j'ai un tableau de taill N (nombre de noeud), la case i m'indique
// si on m'a déjà envoyé un message.
// Lorsque je reçoit un message je regarde ma case dans le tableau, si je suis à
// un je change pas de couleur.
// Sinon je défini une couleur aléatoire.
public class NoeudCountMessageSentAndColorChild extends Node {
  private boolean changeCouleur; // if true, already received message and changed color, false otherwise.
  private int parent;
  private static int compteurAllMessageSent = 0;
  private static int compteurAllMessageReceived = 0;

  public void onStart() {
    changeCouleur = false; // onStart est souvent utilisé pour initialiser les variables
    parent = -1;
  }

  public void onSelection() {
    System.out.println("Vous avez sélectionné le noeud numéro " + getID() + "!");
    Message m = new Message("Bonjour voisin!");
    // Ici nous utilisons des chaînes, mais un message peut contenir n’importe quel
    // objet.
    sendAll(m); // sendAll envoie le message à tous mes voisins
    changeCouleur = true;
    parent = getID();
    setColor(Color.RED); // Root node
    NoeudCountMessageSentAndColorChild.compteurAllMessageSent += getNeighbors().size();
    NoeudCountMessageSentAndColorChild.compteurAllMessageReceived++;
  }

  // Cette méthode est appelée lors de la réception d’un message
  public void onMessage(Message m) {
    if (changeCouleur) { // J'ai déjà changé ma couleur donc déjà reçu un message.
      // Est-ce que je suis le dernier noeud ?
      final int numberOfNodesInSystem = getTopology().getNodes().size(); // Nombre de noeud dans le système (pas le
                                                                         // nombre de noeud qui ont reçu un message)
      if (numberOfNodesInSystem == NoeudCountMessageSentAndColorChild.compteurAllMessageReceived)
        System.out.println("Dernier noeud à avoir reçu le message " + getID() + " et le compteur globale " + NoeudCountMessageSentAndColorChild.compteurAllMessageSent);
      return;
    }
    // On reçoit pour la première fois un message alors on change de couleur.
    changeCouleur = true;
    parent = m.getSender().getID();
    // getID() pour avoir mon id.
    System.out.println("Le noeud " + getID() + " a reçu le message suivant du noeud "
        + m.getSender() + ": \"" + m.getContent() + "\"");
    setColor(Color.BLUE); // Child
    sendAll(m); // sendAll envoie le message à tous mes voisins

    // Comme le nombre de voisin correspond au nombre de message que
    // l'on a envoyé on utilise simplement cette valeur locale comme
    // référence du nombre de messsage envoyé.
    NoeudCountMessageSentAndColorChild.compteurAllMessageSent += getNeighbors().size();

    // On incrémente pour savoir combien de noeud ont déjà reçu un message afin
    // de savoir à la fin si tout le monde a bien reçu le message.
    NoeudCountMessageSentAndColorChild.compteurAllMessageReceived++;
  }
}
