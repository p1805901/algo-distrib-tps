import io.jbotsim.core.Color;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

public class NoeudTest extends Node {
  public void onStart() { // Cette méthode est appelée lorsqu'un nouveau noeud est ajouté au graphe.
    System.out.println("Le noeud numéro " + this.getID() + " vient d'être crée !");
  }

  public void onSelection() { // Définit la méthode qui va être appelé lorsque l'on va sélectionner un noeud.
    System.out.println("Le noeud numéro " + this.getID() + " a été sélectionné !");
    // Je veux envoyer un message à tous mes voisins.
    // Je créer un message.
    Message msg = new Message("Bonjour mon reuf !");
    // Je l'envoie à tous mes voisins.
    this.sendAll(msg);
    // Je check si j'ai des voisins
    if (this.getNeighbors().size() > 0) {
      final Node monPremierVoisin = this.getNeighbors().get(0);
      this.send(monPremierVoisin, new Message("Wesh mon voisin !"));
    }
  }

  // Je veux à la réception d'un message afficher le noeud qui m'a envoyé un
  // message.
  // Enfin je veux changer de couleur si c'est un de mes voisins dont je suis le
  // premier noeud.
  public void onMessage(Message msg) {
    System.out.println("Le noeud numéro " + this.getID() + " a reçu un message de " + msg.getSender().getID() + " : "
        + msg.getContent());
    // Je change de couleur si le message contient Wesh.
    if(msg.getContent().toString().contains("Wesh")) {
      this.setColor(Color.RED);
    }
  }
}
