import io.jbotsim.core.Color;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

public class NoeudAlgoDiffusion extends Node {

  private int compteurDeMessageRecu;
  private static int compteurDeMessageDejaRecu = 0;
  private static int compteNombreDeMessageEnvoye = 0;

  public void onStart() {
    compteurDeMessageRecu = 0;
    System.out.println("Je suis le Noeud numéro " + this.getID() + " et je viens d'être créé !");
  }

  public void onSelection() {
    System.out.println(
        "Je suis le Noeud numéro " + this.getID() + " et je viens d'être sélectionné pour démarrer la diffusion !");
    Message m = new Message("Bonjour poto !");
    compteurDeMessageRecu++; // On a besoin que la racine soit isolé de ces fils et donc ait déjà un message
                             // reçu pour avoir le nombre de message envoyé exact sinon on aura un envoit de
                             // plus à faire quand un de nos voisins nous retransmettra le message
    for (Node voisin : this.getNeighbors()) {
      this.send(voisin, m);
      compteNombreDeMessageEnvoye++;
    }
  }

  public void onMessage(Message messagRecu) {
    System.out.println("Je suis le Noeud numéro " + this.getID() + " et j'ai reçu un message de "
        + messagRecu.getSender().getID() + " : " + messagRecu.getContent());
    if (compteurDeMessageRecu == 0) { // C'est la première fois que je reçois un message youpi.
      compteurDeMessageRecu++;
      this.setColor(Color.GREEN);
      for (Node voisin : this.getNeighbors()) {
        this.send(voisin, messagRecu);
        compteNombreDeMessageEnvoye++;
      }
    } else {
      compteurDeMessageDejaRecu++;
    }
    if (compteurDeMessageDejaRecu == getTopology().getNodes().size()) {
      System.out.println("Tous les noeuds ont reçu le message !");
      System.out.println("Nombre de message envoyé : " + compteNombreDeMessageEnvoye);
    }
  }
}
