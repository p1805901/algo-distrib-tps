import java.util.ArrayList;
import java.util.List;

import io.jbotsim.core.Color;
import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

public class NoeudTreeCover extends Node {
  private boolean hasReceivedJOIN; // Compteur local pour chaque noeud.
  private int parentID;
  private Node parent;
  private static Node root;
  private List<Node> children;
  private int compteurMessageBackNoReceived;
  private int compteurMessageBackReceived;
  // private static int compteurAllMessageReceived = 0;

  public void onStart() {
    hasReceivedJOIN = false; // onStart est souvent utilisé pour initialiser les variables
    children = new ArrayList<Node>(); // On tien une liste de tous nos enfants.
    compteurMessageBackNoReceived = 0;
    compteurMessageBackReceived = 0;
  }

  public void onSelection() {
    System.out.println("Vous avez sélectionné le noeud numéro " + getID() + "!");
    Message m = new Message("JOIN");
    // Ici nous utilisons des chaînes, mais un message peut contenir n’importe quel
    // objet.
    sendAll(m); // sendAll envoie le message à tous mes voisins
    hasReceivedJOIN = true;
    parentID = getID();
    parent = this;
    root = this;
    setColor(Color.YELLOW); // Root node
  }

  // Cette méthode est appelée lors de la réception d’un message
  public void onMessage(Message m) {
    if (!hasReceivedJOIN && m.getContent().equals("JOIN")) { // Si je reçois un JOIN pour la première fois
      setWhoIsMyParentAndSetTrueJOIN(m);
      changeLinkColorAndWidthWithParent();
      setColor(Color.YELLOW); // JOIN l'arbre.
      printSenderAndMyId(m);

      if (hasReceivedJOIN && getNeighbors().size() == 1) { // Si je suis une feuille.
        final Message messageBACK = new Message("BACK");
        sendBackMessageSinceImLeaf(messageBACK);
        System.out.print("Je suis feuille " + getID());
        return;
      }
      final Message messageJOIN = new Message("JOIN");
      sendJOINMessageToAllOfMyNeighbourExceptParent(messageJOIN);
      return;
    }

    if (hasReceivedJOIN && m.getContent().equals("JOIN")) { // J'ai déjà reçu un message JOIN -> Renvoie un BACKNO
      send(m.getSender(), new Message("BACKNO"));
      return;
    }
    // Si j'ai déjà rejoins l'abre
    checkIfMessageBackIfSoAddSenderAsChild(m);
    checkIfMessageBacknoAndIncrementCounter(m);
    System.out.println("Mon nombre de fils est : " + children.size());
    System.out.println("Nombre de message Back reçu : " + compteurMessageBackReceived);
    if ((compteurMessageBackReceived + compteurMessageBackNoReceived) == children.size() // J'ai reçu un back et backno de tous mes fils.
        && parentID != root.getID()) { // Send a message BACK to the parent.
      send(parent, new Message("BACK"));
      setColor(Color.GREEN);
      return;
    }

    if (NoeudTreeCover.root == this // Is root ?
        && (m.getContent().equals("BACK") || m.getContent().equals("BACKNO")) // Message is wether BACK or BACK NO.
        && (compteurMessageBackReceived + compteurMessageBackNoReceived) == children.size()) // Got a message from all
                                                                                             // of my children.
      System.out.println("Fin de l'algorithme, la racine de cette arbre est : " + getID());
  }

  private void checkIfMessageBacknoAndIncrementCounter(Message m) {
    if (m.getContent().equals("BACKNO"))
      compteurMessageBackNoReceived++;
  }

  private void checkIfMessageBackIfSoAddSenderAsChild(Message m) {
    if (m.getContent().equals("BACK")) {
      System.out.print("Is Message BACK");
      compteurMessageBackReceived++;
      if(!children.contains(m.getSender()))
        children.add(m.getSender());
    }
  }

  private void sendBackMessageSinceImLeaf(final Message messageBACK) {
    send(parent, messageBACK);
    setColor(Color.GREEN);
  }

  private void sendJOINMessageToAllOfMyNeighbourExceptParent(final Message messageJOIN) {
    for (Node neighbor : getNeighbors()) {
      if (neighbor.getID() != parentID) {
        send(neighbor, messageJOIN);
        children.add(neighbor);
        System.out.println("J'ai " + children.size() + " fils ");
      }
    }
  }

  private void printSenderAndMyId(Message m) {
    System.out.println("Le noeud " + getID() + " a reçu le message suivant du noeud "
        + m.getSender() + ": \"" + m.getContent() + "\"");
  }

  private void setWhoIsMyParentAndSetTrueJOIN(Message m) {
    hasReceivedJOIN = true;
    parentID = m.getSender().getID();
    parent = m.getSender();
  }

  private void changeLinkColorAndWidthWithParent() {
    getCommonLinkWith(parent).setColor(Color.BLUE);
    getCommonLinkWith(parent).setWidth(10);
  }
}
